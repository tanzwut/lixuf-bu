//
//  BUApp.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 09/02/21.
//

import SwiftUI
import UIKit

@main
struct BUApp: App {
    
    @StateObject var appState = AppState()
    
    @State var showAddCardSheet = false
    @State var idCard: Int = -1
    
    @State var cards: [CardView] = []
    
    var body: some Scene {
        WindowGroup {
            if appState.currenState == .launching {
                SplashScreen()
                    .environmentObject(appState)
            }
            else if appState.currenState == .login {
                LoginView()
                    .environmentObject(appState)
                    .onAppear {
                        //appState.currenState = .logged
                    }
            }
            else if appState.currenState == .logged {
                SearchView(cards: self.$cards)
                    .environmentObject(appState)
                    .sheet(isPresented: self.$showAddCardSheet, content: {
                        AddCardSheetView(cardId: self.$idCard, onAdded: { tarjeta in
                            if !self.cards.contains(where: { $0.id == tarjeta.id }) {
                                withAnimation {
                                    self.cards.append(tarjeta)
                                }
                            }
                        })
                    })
                    .onOpenURL(perform: { url in
                        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return }
                        
                        let path = components.path
                        
                        //guard let params = components.queryItems else { return }
                        
                        debugPrint("Path: \(path)")
                        
                        if let cardId = path.last {
                            debugPrint("CardID: \(cardId.wholeNumberValue!)")
                            self.idCard = cardId.wholeNumberValue!
                            self.showAddCardSheet = true
                        }
                        
                    })
                    .onContinueUserActivity(NSUserActivityTypeBrowsingWeb) { activity in
                        guard let url = activity.webpageURL, let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return }
                        
                        debugPrint(url)
                        
                        let path = components.path
                        
                        //guard let params = components.queryItems else { return }
                        
                        debugPrint("Path: \(path)")
                        
                        if let cardId = path.last {
                            debugPrint("CardID: \(cardId.wholeNumberValue!)")
                            self.idCard = cardId.wholeNumberValue!
                            self.showAddCardSheet = true
                        }
                    }
            }
        }
    }
}
