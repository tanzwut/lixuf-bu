//
//  CustomButton.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 12/02/21.
//

import SwiftUI

struct CustomButton: ViewModifier {
    
    func body(content: Content) -> some View {
        
        return content
            .foregroundColor(Color("LoginColor"))
            .padding()
            .padding(.horizontal)
            .background(
                LinearGradient(gradient: Gradient(colors: [Color.white]), startPoint: .bottomLeading, endPoint: .topTrailing)
            )
            .clipShape(Capsule())
        
    }
}

