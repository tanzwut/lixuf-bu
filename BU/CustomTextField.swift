//
//  CustomTextField.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 12/02/21.
//

import SwiftUI

struct CustomTextField: View {
    
    var image: String
    var title: String
    @Binding var value: String
    var animation: Namespace.ID
    
    var body: some View {
        
        VStack(spacing: 6){
            
            HStack(alignment: .bottom){
                
                Image(systemName: image)
                    .font(.system(size: 22))
                    .foregroundColor(!value.isEmpty ? .black : .white)
                    .frame(width: 35)
                
                VStack(alignment: .leading, spacing: 6) {
                
                    if value != ""{
                        
                        Text(title)
                            .font(.callout)
                            .fontWeight(.regular)
                            .foregroundColor(.black)
                            .matchedGeometryEffect(id: title, in: animation)
                    }
                    
                    ZStack(alignment: Alignment(horizontal: .leading, vertical: .center) ){
                        
                        if value == ""{
                            
                            Text(title)
                                .font(.callout)
                                .fontWeight(.regular)
                                .foregroundColor(.white)
                                .matchedGeometryEffect(id: title, in: animation)
                        }
                        
                        if title == "Contraseña"{
                            
                            SecureField("", text: $value)
                                .foregroundColor(.black)
                            
                        }
                        else{
                            
                            TextField("", text: $value)
                                //.keyboardType(title == "Telefono" ? .numberPad : .default)
                                .foregroundColor(.black)
                        }
                        
                    }
                }
            }
            
            if value == "" {
                
                Divider()
                
            }
        }
        
        .padding(.horizontal)
        .padding(.vertical,10)
        .background(Color.white.opacity(value != "" ? 1:0))
        .cornerRadius(8)
        .shadow(color: Color.black.opacity(0.5), radius: 5, x: 5, y: 5)
        .shadow(color: Color.black.opacity(0.05), radius: 5, x: -5, y: -5)
        .padding(.horizontal)
        .padding(.top)
        .animation(.linear)
    }
}


