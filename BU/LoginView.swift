//
//  LoginView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 09/02/21.
//

import SwiftUI
import Alamofire
import KeychainSwift
import SwiftyJSON
import ToastUI

struct LoginView: View {
    @EnvironmentObject var appState: AppState

    @State var showLoadingToast = false
    @State var showErrorToast = false
    @State var errorToastMessage = ""
    
    @State var usuario = "api@apibeyou.com"
    @State var password = "apibeyouapi"
    //@State var usuario = ""
    //@State var password = ""
    @State var showRegister = false
    @State var show = false
    @Namespace var animation
    
    var body: some View {
        NavigationView {

            ZStack {
                Color("LoginColor")
                    .edgesIgnoringSafeArea(.all)
                    .toast(isPresented: self.$showLoadingToast) {
                        ToastView("Iniciando sesión...")
                            .toastViewStyle(IndefiniteProgressToastViewStyle())
                    }
                
                VStack {
                    
                    Spacer()
                    
                    Image("BU")
                        .resizable()
                        .frame(width: 100, height: 100, alignment: .center)
                        .padding(.bottom)
                    
                    CustomTextField(image: "person", title: "Usuario", value: $usuario, animation: animation)
                        .keyboardType(.emailAddress)
                    
                    CustomTextField(image: "lock", title: "Contraseña", value: $password, animation: animation)
                    
                    Button("¿Olvidaste tu contraseña?") {
                        
                    }
                    .font(.footnote)
                    .foregroundColor(.white)
                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
                    .padding(.horizontal)
                    .padding(.vertical)
                    
                    Button(action: {
                        //appState.currenState = .logged
                        if usuario.isEmpty || password.isEmpty {
                            errorToastMessage = "Usuario/Contraseña incorrectos"
                            showErrorToast.toggle()
                        }
                        else if (!textFieldValidatorEmail(usuario)){
                            errorToastMessage = "Correo no valido"
                            showErrorToast.toggle()
                        }
                        else {
                            self.showLoadingToast = true
                            
                            let userLogin = ["email":self.usuario.trimmingCharacters(in: .whitespacesAndNewlines), "password":self.password.trimmingCharacters(in: .whitespacesAndNewlines)]
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                AF.request(API.getLoginAddress(), method: .post, parameters: userLogin, encoder: JSONParameterEncoder.default).responseString(completionHandler: { response in
                                    switch response.result {
                                    case .success(let data):
                                        let json = JSON(parseJSON: data)
                                        
                                        if let access_token = json["access_token"].string {
                                            let keychain = KeychainSwift()
                                            
                                            keychain.set(access_token, forKey: "BUiOSAppAccessToken")
                                            
                                            self.showLoadingToast = false
                                            appState.currenState = .logged
                                        }
                                        else {
                                            self.showLoadingToast = false
                                            self.errorToastMessage = "Usuario/Contraseña incorrectos."
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                                                self.showErrorToast = true
                                            }
                                        }
                                    case .failure(let error):
                                        debugPrint(error)
                                        self.showLoadingToast = false
                                    }
                                })
                            }
                        }
                    }) {
                        HStack (spacing: 10) {
                            
                            Text("Iniciar Sesion")
                                .fontWeight(.heavy)
                        }
                        .modifier(CustomButton())
                    }
                   
                 
                    Spacer()
                    HStack {
                        
                        Text("¿No tienes una cuenta?")
                            .font(.callout)
                            .foregroundColor(.white)
                        NavigationLink(
                            destination: Register(show: $showRegister).environmentObject(appState),
                            isActive: $showRegister,
                            label: {
                                Text("Registrate")
                                    .font(.callout)
                                    .foregroundColor(.black)
                            })
                    }
                    .hidden()
                }
                .toast(isPresented: self.$showErrorToast, dismissAfter: 2.0) {
                    ToastView(self.errorToastMessage)
                        .toastViewStyle(ErrorToastViewStyle())
                }
            }
        }
    }
    
    func textFieldValidatorEmail(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        //let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
