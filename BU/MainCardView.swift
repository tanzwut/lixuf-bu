//
//  MainCardView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 10/02/21.
//

import SwiftUI
import EFQRCode
import URLImage
import URLImageStore

struct MainCardView: View {
    
    @State private var showShareSheet = false
    var cardView: CardView
    
    let urlImageService = URLImageService(fileStore: URLImageFileStore(), inMemoryStore: URLImageInMemoryStore())
    
    var body: some View {
        
        VStack {
            
            //Color(.white)
               // .edgesIgnoringSafeArea(.all)
            HStack{
                /*if cardView.image != nil {
                    Image(uiImage: UIImage(data: cardView.image!)!)
                        .resizable() //Adaptar el tamaño de la imagen
                        .frame(width: 100, height: 100)
                        .clipShape(Circle())
                        .padding()
                }
                else {
                    Image("BU")
                        .resizable() //Adaptar el tamaño de la imagen
                        .frame(width: 100, height: 100)
                        .clipShape(Circle())
                        .padding()
                }*/
                
                if let image = cardView.image, let imageUrl = URL(string: image) {
                    URLImage(imageUrl, empty: {
                        EmptyView()
                    }, inProgress: { progress in
                        ActivityIndicator()
                    }, failure: { (error, retry) in
                        VStack {
                            Text(error.localizedDescription)
                            Button("Retry", action: retry)
                        }
                    }) { image in
                        image
                            .resizable() //Adaptar el tamaño de la imagen
                            .frame(width: 100, height: 100)
                            .clipShape(Circle())
                            .aspectRatio(contentMode: .fill)
                            .padding()
                    }
                }
                else {
                    Image("BU")
                        .resizable() //Adaptar el tamaño de la imagen
                        .frame(width: 100, height: 100)
                        .clipShape(Circle())
                        .padding()
                }
                
                
                Spacer()
                
                /*(
                    Text(String(cardView.qualification ?? 5.0))
                        .foregroundColor(.white)
                        +
                    Text(Image(systemName: "star.fill"))
                        .foregroundColor(.yellow)
                )
                    .padding(.horizontal)*/
                Image(uiImage: UIImage(cgImage: EFQRCode.generate(for: "https://solyfel.com/share/\(cardView.id!)", encoding: .ascii, size: .init(width: 100, height: 100), backgroundColor: Color.clear.cgColor!, foregroundColor: Color.white.cgColor!)!))
                    .resizable()
                    .frame(width: 100, height: 100, alignment: .center)
                    .padding(.horizontal)
                
            }
            
                Text(cardView.fullname)
                    .foregroundColor(Color.white) //Cambiar el color de la fuente
                    .padding(.horizontal)
                
                Text(cardView.profession)
                    .foregroundColor(Color.white) //Cambiar el color de la fuente
                    //.padding()
                
                Spacer()
                
                HStack{
                    
                    /*Text(cardView.rfc ?? "")
                    .font(.callout)
                    .foregroundColor(Color.white)
                    .fixedSize(horizontal: false, vertical: true)*/
                    
                    if cardView.hipervinculo != nil {
                        Button(action: {
                            if let vinculo = cardView.hipervinculo, let url = URL(string: vinculo) {
                                UIApplication.shared.open(url)
                            }
                        }, label: {
                            Image(systemName: "bag")
                                .font(.system(size: 22))
                                .foregroundColor(Color.white)
                        })
                    }
                    
                    Spacer()
                
                    Button(action: {
                            self.showShareSheet = true;
                    }, label: {
                        Image(systemName: "square.and.arrow.up")
                            .foregroundColor(Color.white)
                            .font(.system(size: 22))
                    })
                            .sheet(isPresented: $showShareSheet) {
                                ShareSheet(activityItems: ["https://solyfel.com/share/\(cardView.id!)"]) { (_, completed, _, error) in
                                    if completed {
                                        self.showShareSheet = false
                                    }
                                }
                            }
                    
                    if cardView.phone != nil {
                        Button(action: {
                            let telephone = "tel://"
                            let formattedString = telephone + cardView.phone!
                            guard let url = URL(string: formattedString) else { return }
                            UIApplication.shared.open(url)
                        }, label: {
                            Image(systemName: "phone")
                                .font(.system(size: 22))
                                .foregroundColor(Color.white)
                        })
                    }
                    
                    Button(action: {
                        let mail = "mailto:"
                        let formattedString = mail + cardView.mail
                        guard let url = URL(string: formattedString) else { return }
                        UIApplication.shared.open(url)
                    }, label: {
                        Image(systemName: "envelope")
                            .font(.system(size: 22))
                            .foregroundColor(Color.white)
                    })
                    
                    if cardView.whatsapp != nil {
                        Button(action: {
                            let whatsapp = "whatsapp://send?phone="
                            let formattedString = whatsapp + cardView.whatsapp!
                            guard let url = URL(string: formattedString) else { return }
                            UIApplication.shared.open(url)
                        }, label: {
                            Image("whatsapp")
                                .resizable()
                                .frame(width: 28, height: 28)
                                .colorInvert()
                        })
                    }
                    
                    if cardView.address != nil {
                        Button(action: {
                            let address = "https://maps.apple.com/?q="
                            let formattedString = address + cardView.address!.replacingOccurrences(of: " ", with: "+")
                            guard let url = URL(string: formattedString) else { return }
                            UIApplication.shared.open(url)
                        }, label: {
                            Image(systemName: "mappin")
                                .font(.system(size: 22))
                                .foregroundColor(Color.white)
                        })
                    }
                }
                .padding()
            
        }
        .frame(height: 225)
        .background(Color.gray.opacity(0.6))
        .cornerRadius(10)
        .padding(.horizontal, 8)
        .environment(\.urlImageService, urlImageService)
    }
    
       
}

struct MainCardView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
