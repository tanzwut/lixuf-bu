//
//  CardView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 10/02/21.
//

import Foundation
import ObjectMapper

class CardView: Mappable, Identifiable {
    var id: Int!
    var image: String?
    var fullname: String!
    var profession: String!
    var address: String?
    var qualification: Double?
    var phone: String?
    var mail: String!
    var whatsapp: String?
    var hipervinculo: String?
    var rfc: String?
    
    // Datos para facturación
    var companyName: String = "Lixuf S.A. de C.V."
    var companyStreet: String = "Calle Lixuf 123"
    var companyStreet2: String = "Colonia Lixuf"
    var companyCity: String = "Zapopan"
    var companyState: String = "Jalisco"
    var companyCP: String = "44545"
    var companyRfc: String = "LIXU123456EA2"
    var companyEmail: String = "lixuf@lixuf.com"
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        fullname <- map["nombre"]
        profession <- map["puesto"]
        mail <- map["email"]
        phone <- map["contacto"]
        whatsapp <- map["whatsapp"]
        image <- map["imagen"]
        hipervinculo <- map["hipervinculo"]
        
        //TO-DO: agregar el mapeo de los datos de facturación enviados por el servidor
    }
}
