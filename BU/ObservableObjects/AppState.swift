//
//  AppState.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 12/02/21.
//

import SwiftUI

class AppState: ObservableObject {
    
    enum AppStates {
        case login, logged, launching
    }
    
    @Published var currenState: AppStates = .launching
    
}
