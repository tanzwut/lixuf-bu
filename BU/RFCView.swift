//
//  RFCView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 19/05/21.
//

import SwiftUI
import EFQRCode

struct RFCView: View {
    @Environment(\.presentationMode) var presentationMode
    var cardView: CardView
    
    var body: some View {
        ZStack {
            Color("LoginColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                Image(uiImage: UIImage(cgImage: EFQRCode.generate(for: "\(self.cardView.companyName), \(self.cardView.companyStreet), \(self.cardView.companyStreet2), \(self.cardView.companyCP), \(self.cardView.companyCity), \(self.cardView.companyState) \(self.cardView.companyRfc) \(self.cardView.companyEmail)", encoding: .utf8, size: .init(width: 200, height: 200), magnification: .none, backgroundColor: Color.clear.cgColor!, foregroundColor: Color.white.cgColor!)!))
            }
            .navigationBarTitle(Text("Datos RFC"))
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack(spacing: 5) {
                    Image(systemName: "chevron.left")
                        .foregroundColor(.white)
                        .font(.title)
                    
                    Text("Regresar")
                        .foregroundColor(.white)
                }
            })
        }
    }
}

struct RFCView_Previews: PreviewProvider {
    static var previews: some View {
        RFCView(cardView: CardView())
    }
}
