//
//  Register.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 12/02/21.
//

import SwiftUI

struct Register: View {
    @EnvironmentObject var appState: AppState
    
    @State var email = ""
    @State var password = ""
    @State var fullname = ""
    @State var adress = ""
    @State var phone = ""
    @Namespace var animation
    
    @Binding var show: Bool
    
    var body: some View {
        
        ZStack{
            Color("LoginColor")
                .edgesIgnoringSafeArea(.all)
        
        VStack{
            
            HStack{
                
                Button(action: {show.toggle()}) {
                    
                    Image(systemName: "arrow.left")
                        .font(.largeTitle)
                        .foregroundColor(.white)
                }
                Spacer()
            }
            .padding()
            
            ScrollView {
                HStack {
                    
                    Text("Crea una cuenta")
                        .font(.system(size: 40))
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        Spacer(minLength: 0)
                }
                .padding()
                
                
                CustomTextField(image: "person", title: "Nombre", value: $fullname, animation: animation)
                
                CustomTextField(image: "envelope", title: "Correo", value: $email, animation: animation)
                    .keyboardType(.emailAddress)
                    .padding(.top,5)
                
                CustomTextField(image: "mail", title: "Direccion", value: $adress, animation: animation)
                    .padding(.top,5)
                
                CustomTextField(image: "phone.fill", title: "Telefono", value: $phone, animation: animation)
                    .keyboardType(.phonePad)
                    .padding(.top,5)
                
                CustomTextField(image: "lock", title: "Contraseña", value: $password, animation: animation)
                    .padding(.top,5)
                
                HStack {
                    
                    Spacer()
                    
                    Button(action: {
                        appState.currenState = .logged
                    }) {
                        HStack (spacing: 10) {
                            
                            Text("Registrarse")
                                .fontWeight(.heavy)
                            
                            Image(systemName: "arrow.right")
                                .font(.title2)
                        }
                        .modifier(CustomButton())
                    }
                }
                .padding()
                .padding(.top)
                .padding(.trailing)
                Spacer(minLength: 0)
            }
            
        }
        .navigationTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    }
}

struct Register_Previews: PreviewProvider {
    static var previews: some View {
        Register( show: .constant(true))
    }
}
