//
//  SearchView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 10/02/21.
//

import SwiftUI
import Alamofire
import KeychainSwift
import SwiftyJSON
import AlamofireObjectMapper
import SwipeCell

struct SearchView: View {
    
    @State var searchText = ""
    @State var isSearching = false
    //@State var cards: [CardView] = [CardView(image: Data(), fullname: "Pedro Emmanuel Ayala Rodriguez", profession: "Ing en Computacion", address: "Volcan Telica 5130, Lomas del Paraiso", qualification: 4.8, phone: "523322870565", mail: "emmanuel12345@hotmail.com", whatsapp: "523322870565", rfc: "AARP900926EY1"), CardView(image: Data(), fullname: "Arturo de la Torre Quezada", profession: "Ing en Computacion", address: "Volcan Telica 5130, Lomas del Paraiso", qualification: 5.0, phone: "523322870565", mail: "emmanuel12345@hotmail.com", whatsapp: "523322870565", rfc: "VICB960603JN1"), CardView(image: Data(), fullname: "Brenda Graciela Vivian Chavez", profession: "Ing en Electronica", address: "Sendero de las cumbres 110, Senderos de Monteverde", qualification: 4.9, phone: "523315846536", mail: "brenda_vivian@hotmail.com", whatsapp: "523315846536", rfc: "VICB960603JN1")]
    @Binding var cards: [CardView]
    
    @State var showDeleteConfirmation = false
    @State var cardToDelete = -1
    
    @State var showRfcInfo = false
    @State var selectedCardView: CardView = CardView()
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("LoginColor")
                    .edgesIgnoringSafeArea(.all)
                    .alert(isPresented: self.$showDeleteConfirmation) {
                        Alert(title: Text("Advertencia"), message: Text("¿Estás seguro de que deseas eliminar esta tarjeta?"), primaryButton: .destructive(Text("Eliminar"), action: {
                            self.removerTarjeta(cardId: self.cardToDelete, onSuccess: { status in
                                if status == "Tarjeta eliminada correctamente" {
                                    withAnimation {
                                        self.cards.removeAll(where: { $0.id == self.cardToDelete })
                                    }
                                }
                            })
                        }), secondaryButton: .cancel(Text("Cancelar")))
                    }
                
                ScrollView {
                    LazyVStack {
                        NavigationLink(
                            destination: RFCView(cardView: self.selectedCardView),
                            isActive: self.$showRfcInfo,
                            label: {
                                EmptyView()
                            })
                        
                        SearchBar(searchText: $searchText, isSearching: $isSearching)
                        
                        ForEach(cards.filter({$0.fullname.contains(searchText) || searchText.isEmpty})){
                            num in
                                
                            MainCardView(cardView: num)
                                .onSwipe(leading: [Slot(image: { Image(systemName: "qrcode") }, title: { AnyView(Text("RFC").foregroundColor(.white)) }, action: {
                                    self.selectedCardView = num
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        self.showRfcInfo.toggle()
                                    }
                                }, style: .init(background: .orange, imageColor: .white, slotWidth: 90))], trailing: [Slot(image: { Image(systemName: "xmark.circle.fill") }, title: { AnyView(Text("Eliminar").bold().foregroundColor(.white)) }, action: {
                                    self.cardToDelete = num.id
                                    
                                    if self.cardToDelete > 0 {
                                        self.showDeleteConfirmation.toggle()
                                    }
                                }, style: .init(background: .red, imageColor: .white, slotWidth: 90))])
                        }.padding(.vertical, 4)
                    }
                }
                .navigationBarTitle(Text("Contactos"))
                .onAppear {
                    let navBarAppearance = UINavigationBar.appearance()
                    navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                    navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                    navBarAppearance.barTintColor = UIColor(named: "LoginColor")!
                }
            }
        }
        .onTapGesture {
            UIApplication.shared.sendAction(#selector(UIResponder .resignFirstResponder), to: nil, from: nil, for: nil)
        }
        .onAppear {
            fetchCards()
        }
    }
    
    func fetchCards() {
        let keychain = KeychainSwift()
        guard let access_token = keychain.get("BUiOSAppAccessToken") else { return }
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: access_token),
            .accept("application/json")
        ]
        
        AF.request(API.getWalletAddress(), method: .get, headers: headers).responseArray { (response: DataResponse<[CardView], AFError>) in
            switch response.result {
            case .success(let data):
                self.cards = data
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func removerTarjeta(cardId: Int, onSuccess: @escaping (String)->Void, onError: ((String)->Void)? = nil) {
        let keychain = KeychainSwift()
        guard let access_token = keychain.get("BUiOSAppAccessToken") else { return }
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: access_token),
            .accept("application/json")
        ]
        
        let parameters = [
            "ID_tarjeta": cardId
        ]
        
        AF.request(API.removeCardAddress(), method: .post, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                //self.toastMessage = json["status"].string ?? "No response data"
                if let status = json["status"].string {
                    onSuccess(status)
                }
            case .failure(let error):
                if let onError = onError {
                    onError(error.localizedDescription)
                }
            }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(cards: .constant([CardView]()))
    }
}

struct SearchBar: View {
    
    @Binding var searchText: String
    @Binding var isSearching: Bool
    @Namespace var animation
    
    var body: some View {
        
        HStack{
            HStack{
                TextField("Buscar", text: $searchText, onEditingChanged: { editing in
                    isSearching = editing
                })
                   .foregroundColor(.black)
                   .padding(.leading, 24)
            }
            .padding()
            //.background(Color(.systemGray4))
            .background(Color.white)            .
             cornerRadius(10)
            .padding(.horizontal, 8)
            /*.onTapGesture (perform: {
                isSearching = true
            })*/
            .overlay(
                HStack{
                    Image(systemName: "magnifyingglass")
                    Spacer()
                    if isSearching{
                        Button(action: {searchText = ""}, label: {
                            Image(systemName: "xmark.circle.fill")
                                .padding(.vertical)
                        })
                    }
                }
                .padding(.horizontal, 24)
                .foregroundColor(.gray)
            )
            .transition(.move(edge: .trailing))
            .animation(.spring())
            
            if isSearching {
                Button(action: {
                    isSearching = false
                    searchText = ""
                    //Quitar el teclado cuando presionas cancelar
                    UIApplication.shared.sendAction(#selector(UIResponder .resignFirstResponder), to: nil, from: nil, for: nil)
                    
                }, label: {
                    Text("Cancelar")
                        .foregroundColor(.white)
                        .padding(.trailing)
                        .padding(.leading, 0)
                })
                .transition(.move(edge: .trailing))
                .animation(.spring())
            }
        }
    }
}
