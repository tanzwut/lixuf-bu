//
//  SplashScreen.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 16/02/21.
//

import SwiftUI
import Alamofire

struct SplashScreen: View {
    @EnvironmentObject var appState: AppState
    
    @State var animate = false
    @State var endSplash = false
    
    var body: some View {
            
            ZStack{
                
                Color("LoginColor")
                Image("BU")
                    .resizable()
                    .renderingMode(.original)
                    .aspectRatio(contentMode: animate ? .fill : .fit)
                    .frame(width: animate ? nil: 85, height: animate ? nil: 85)
                    
                    .scaleEffect(animate ? 3: 1)
                    .frame(width: UIScreen.main.bounds.width)
            }
            .ignoresSafeArea(.all, edges: .all)
            .onAppear(perform: animateSplash)
            .opacity(endSplash ? 0:1)
    }
    func animateSplash(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25){
            withAnimation(Animation.easeOut(duration: 0.55)){
                animate.toggle()
            }
            
            withAnimation(Animation.easeOut(duration: 0.55)){
                endSplash.toggle()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.33) {
                    appState.currenState = .login
                }
            }
        }
    }
}

struct SplashScreen_Previews: PreviewProvider {
    static var previews: some View {
        SplashScreen()
    }
}


