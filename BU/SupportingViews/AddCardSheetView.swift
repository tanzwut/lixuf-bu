//
//  AddCardSheetView.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 11/03/21.
//

import SwiftUI
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import KeychainSwift
import ToastUI

struct AddCardSheetView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @State var card: CardView? = nil
    
    @State var showSuccessToast = false
    @State var showErrorToast = false
    @State var showLoadingToast = false
    @State var toastMessage = ""
    
    @Binding var cardId: Int
    
    var onAdded: (CardView)->Void
    
    var body: some View {
        ZStack {
            Color("LoginColor")
                .ignoresSafeArea()
                .toast(isPresented: self.$showLoadingToast) {
                    ToastView(self.toastMessage)
                        .toastViewStyle(IndefiniteProgressToastViewStyle())
                }
            
            VStack {
                HStack {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("Cancelar")
                            .foregroundColor(.white)
                            .padding()
                    })
                    .toast(isPresented: self.$showSuccessToast, dismissAfter: 2.0, onDismiss: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        ToastView(self.toastMessage)
                            .toastViewStyle(SuccessToastViewStyle())
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.toastMessage = "Agregando tarjeta..."
                        self.showLoadingToast = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            
                            self.showLoadingToast = false
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                self.agregarTarjeta(cardId: self.cardId, onSuccess: { status in
                                    self.toastMessage = status
                                    self.showSuccessToast = true
                                    
                                    if let tarjeta = self.card {
                                        onAdded(tarjeta)
                                    }
                                }, onError: { error in
                                    self.toastMessage = error
                                    self.showErrorToast = true
                                })
                            }
                        }
                    }, label: {
                        Text("Agregar")
                            .foregroundColor(.white)
                            .padding()
                    })
                    .toast(isPresented: self.$showErrorToast, dismissAfter: 2.0) {
                        ToastView(self.toastMessage)
                            .toastViewStyle(ErrorToastViewStyle())
                    }
                }
                
                Spacer()
            }
            
            if card != nil {
                MainCardView(cardView: card!)
            }
        }
        .navigationBarTitle(Text(""))
        .navigationBarItems(leading: Button("Cancelar") {
            self.presentationMode.wrappedValue.dismiss()
        }.foregroundColor(.white), trailing: Button("Agregar") {
            
        }.foregroundColor(.white))
        .onAppear {
            let keychain = KeychainSwift()
            guard let access_token = keychain.get("BUiOSAppAccessToken") else { return }
            
            let headers: HTTPHeaders = [
                .authorization(bearerToken: access_token),
                .accept("application/json")
            ]
            
            
            AF.request(API.getCardAddress(cardId: String(self.cardId)), method: .get, headers: headers).responseObject { (response: DataResponse<CardView, AFError>) in
                switch response.result {
                case .success(let card):
                    self.card = card
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            }
        }
    }
    
    func agregarTarjeta(cardId: Int, onSuccess: @escaping (String)->Void, onError: ((String)->Void)? = nil) {
        let keychain = KeychainSwift()
        guard let access_token = keychain.get("BUiOSAppAccessToken") else { return }
        
        let headers: HTTPHeaders = [
            .authorization(bearerToken: access_token),
            .accept("application/json")
        ]
        
        let parameters = [
            "ID_tarjeta": cardId
        ]
        
        AF.request(API.addCardAddress(), method: .post, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                self.toastMessage = json["status"].string ?? "No response data"
                if let status = json["status"].string {
                    onSuccess(status)
                }
            case .failure(let error):
                if let onError = onError {
                    onError(error.localizedDescription)
                }
            }
        }
    }
}

struct AddCardSheetView_Previews: PreviewProvider {
    static var previews: some View {
        AddCardSheetView(cardId: .constant(0), onAdded: { _ in})
    }
}
