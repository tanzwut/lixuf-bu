//
//  ImageTransform.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 08/03/21.
//

import ObjectMapper

class ImageTransform: TransformType {
    typealias Object = Data
    typealias JSON = String
    
    func transformFromJSON(_ value: Any?) -> Data? {
        if let imageString = value as? String {
            if let imageData = Data(base64Encoded: imageString, options: .ignoreUnknownCharacters) {
                return imageData
            }
        }
        
        return nil
    }
    
    func transformToJSON(_ value: Data?) -> String? {
        if let imageData = value {
            return imageData.base64EncodedString(options: .lineLength64Characters)
        }
        
        return nil
    }
}
