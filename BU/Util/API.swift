//
//  API.swift
//  BU
//
//  Created by Emmanuel Rodriguez on 11/03/21.
//

import Foundation

struct API {
    //fileprivate static let baseURL = "http://192.168.100.100:8000/"
    //fileprivate static let baseURL = "https://www.solyfel.com/"
    fileprivate static let baseURL = "http://solyfel.lixuf.com/"
    fileprivate static let loginAddress = "api/login"
    fileprivate static let cardAddress = "api/card"
    fileprivate static let addAddress = "api/addCard"
    fileprivate static let removeAddress = "api/removeCard"
    fileprivate static let walletAddress = "api/wallet"
    
    static func getLoginAddress() -> String {
        return baseURL + loginAddress
    }
    
    static func getCardsAddress() -> String {
        return baseURL + cardAddress
    }
    
    static func getCardAddress(cardId: String) -> String {
        let formattedString = String(format: API.getCardsAddress() + "/%@", cardId)
        return formattedString
    }
    
    static func getWalletAddress() -> String {
        return baseURL + walletAddress
    }
    
    static func addCardAddress() -> String {
        return baseURL + addAddress
    }
    
    static func removeCardAddress() -> String {
        return baseURL + removeAddress
    }
}
